import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp1()));
}

class FakeHttpClient {
  Future<String> get(String url) async {
    await Future.delayed(const Duration(seconds: 1));
    return "response from $url";
  }
}

final fakeHttpClientProvider = Provider((ref) => FakeHttpClient());

final responseProvider =
    FutureProvider.autoDispose.family<String, String>((ref, url) async {
  final httpClient = ref.read(fakeHttpClientProvider);
  return httpClient.get(url);
});

class MyApp1 extends StatelessWidget {
  const MyApp1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(">>>>>>>>>>>> BUILD >>>>>>>>>>>>");
    return MaterialApp(
      title: 'Flutter Riverpod Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: Consumer(
            builder: (context, ref, child) {
              final responseAsyncValue =
                  ref.watch(responseProvider("https://google.com"));
              return responseAsyncValue.map(
                  data: (_) => Text(_.value),
                  error: (_) => Text(_.error.toString(),
                      style: const TextStyle(color: Colors.red)),
                  loading: (_) => const CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}

// ! ===========================================================================

class IncrementNotifier extends ChangeNotifier {
  int _value = 0;
  int get value => _value;

  void increment() {
    _value++;
    notifyListeners();
  }
}

final incrementProvider = ChangeNotifierProvider((ref) => IncrementNotifier());

class MyApp2 extends ConsumerWidget {
  const MyApp2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    print(">>>>>>>>>>>> BUILD >>>>>>>>>>>>");
    return MaterialApp(
      title: 'Flutter Riverpod Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: Consumer(builder: (context, ref, child) {
            final incrementNotifier = ref.watch(incrementProvider);
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(incrementNotifier._value.toString()),
                TextButton(
                    onPressed: () {
                      ref.refresh(incrementProvider);
                    },
                    child: const Text("refresh"))
              ],
            );
          }),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            //context.read(incrementProvider).increment(); // < 1.0.0
            //final incrementNotifier = ref.watch(incrementProvider.notifier); // calls build...
            final incrementNotifier = ref.read(incrementProvider);
            incrementNotifier.increment();
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
